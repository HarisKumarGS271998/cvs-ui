import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { urlConstants } from 'src/app/constants/url-constants';
import { ErrorHandlerComponent } from 'src/app/components/error-handler/error-handler.component';
import { MatDialog } from '@angular/material/dialog';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  searchLocationCode: string = "";

  selectedFilterItems_Dashboard: any = [];
  dropdownData_Dashboard: any[] = [
    // {
    //   'lable': 'None',
    //   'value': 0
    // },
    // {
    //   'lable': 'Personal Information',
    //   'value': 1
    // },
    {
      'lable': 'Location',
      'value': 2
    },
    {
      'lable': 'Office Hours',
      'value': 3
    },
    {
      'lable': 'Maintenance Hours',
      'value': 4
    }
  ];
  applied_Filters_Dashboard: any[] = [];


  selectedFilterItems_Location: any = [];
  dropdownData_Location: any[] = [
    {
      'lable': 'Critical Recertifications',
      'value': 1
    },
    {
      'lable': 'Pending Recertifications',
      'value': 2
    },
    {
      'lable': 'Pending for Approvals',
      'value': 3
    },
    {
      'lable': 'Certified Locations',
      'value': 4
    }
  ];
  applied_Filters_Location: any[] = [];

  isViewLocationDetails: boolean = false;
  isViewDetailRequest: boolean = false;

  // initial center position for the map
  lat: number = 51.673858;
  lng: number = 7.815982;
  zoom: number = 10;
  dashboardData: any = {};
  viewRequestedInfo: any = {};
  siteDetails: any = {};
  CertificationStatusMessage: string = "";
  certificationStatus: boolean = false;
  approved: boolean = false;
  approvalMessage: string = "";
  allLocationData: any = {};

  paginations: any = {
    dashboard: {
      currentPage: 1,
      itemsPerPage: 5
    },
    location: {
      currentPage: 1,
      itemsPerPage: 7
    }
  }

  spinner: boolean = false;

  constructor(private _router: Router, public _http: HttpClient,
    private dialog: MatDialog) { }


  /******************* Common HTTP Methods ************************/

  /**
   * To GET HTTP Requests
   * @param URL 
   * @param Options 
   * @returns 
   */
  doGET(URL: string, Options?: any) {
    return this._http.get(URL, Options);
  }

  /**
   * To POST HTTP Requests
   * @param URL 
   * @param Payload 
   * @param Options 
   * @returns 
   */
  doPOST(URL: string, Payload: any, Options?: any) {
    return this._http.post(URL, Payload, Options);
  }

  /**
   * To PUT HTTP Requests
   * @param URL 
   * @param Payload 
   * @param Options 
   * @returns 
   */
  doPUT(URL: string, Payload: any, Options?: any) {
    return this._http.put(URL, Payload, Options);
  }

  /**
   * To PATCH HTTP Requests
   * @param URL 
   * @param Payload 
   * @param Options 
   * @returns 
   */
  doPATCH(URL: string, Payload: any, Options?: any) {
    return this._http.patch(URL, Payload, Options);
  }

  /**
   * To DELETE HTTP Requests
   * @param URL 
   * @param Options 
   * @returns 
   */
  doDELETE(URL: string, Options?: any) {
    return this._http.delete(URL, Options);
  }



  /******************* Common Reusable Methods ************************/

  /**
   * Navigate Route to given page
   * @param pageName 
   */
  redirectTo(pageName: any) {
    this._router.navigate([pageName]);
  }


  /**
   * Set Selected Tile Items 
   * @param tileItem 
   */
  selectedTileLocation(tileItem: any) {
    this.selectedFilterItems_Location = [tileItem];
    this.redirectTo('admin/locations');

    this.applied_Filters_Location = this.dropdownData_Location.filter(obj => obj.value == tileItem);
  }




  /**
  * Get Dashboard Info
  **/

  getDashboardInfo(page: any, size: any) {

    // debugger;
    let payload: any = this.applied_Filters_Dashboard.map(obj => obj.value) || [];

    let requestURL = urlConstants.baseUrl + urlConstants.dashboardInfo;
    requestURL = requestURL.replace("{page}", page);
    requestURL = requestURL.replace("{size}", size);

    this.spinner = true;
    this.doPOST(requestURL, payload).subscribe((result: any) => {
      this.dashboardData = result || {};
      this.spinner = false;
    },
      (err: any) => {
        console.log(err);
        this.spinner = false;
      })

  }


  getSiteDetails(siteId: string): any {
    let requestURL = urlConstants.baseUrl + urlConstants.getSiteDetails;
    requestURL = requestURL.replace("{siteId}", siteId);
    this.spinner = true;
    this.doGET(requestURL).subscribe((result: any) => {
      this.siteDetails = result || {};
      this.isViewLocationDetails = true;
      this.spinner = false;
    },
      (err: any) => {
        this.spinner = false;
        console.log(err);
      })
  }



  /**
  * Get Dashboard Info
  **/

  getRequestedInfo(logId: any) {

    let requestURL = urlConstants.baseUrl + urlConstants.viewRequestedInfo;
    requestURL = requestURL.replace("{logId}", logId);

    this.spinner = true;
    this.doGET(requestURL).subscribe((result: any) => {

      this.viewRequestedInfo = result || [];

      this.spinner = false;
      if (this.viewRequestedInfo?.logType == 3) {
        this.viewRequestedInfo.oldValues.sort((a: any, b: any) => {
          return a.DayOfWeek - b.DayOfWeek;
        })
        this.viewRequestedInfo.newValues.sort((a: any, b: any) => {
          return a.DayOfWeek - b.DayOfWeek;
        })
      }
    },
      (err: any) => {
        this.spinner = false;
        console.log(err);
      })

  }

  certifySite(payLoad: any): any {
    let requestUrl = urlConstants.baseUrl + urlConstants.certifySite;

    console.log(payLoad);
    this.spinner = true;

    // setTimeout(() => {
    //   this.spinner = false;
    //   this.certificationStatus = true;
    // },3000)
    // return

    this.doPOST(requestUrl, payLoad).subscribe((data: any) => {
      this.spinner = false;
      this.certificationStatus = true;
      this.CertificationStatusMessage = "Your recertification request were sent"
    }, (err: any) => {
      this.spinner = false;
      if (err.status == 400) {
        this.certificationStatus = true;
        this.CertificationStatusMessage = "Current Survey still not in Pending status."
      }
      else {
        this.certificationStatus = true;
        this.CertificationStatusMessage = "Something went wrong.";
      }
    })
    // this.CertificationStatusMessage = "Your recertification request were sent"
    // this.certificationStatus = true;
  }

  updateRequest(payLoad: any): any {
    let requestUrl = urlConstants.baseUrl + urlConstants.updateRequest;

    this.spinner = true;

    this.doPOST(requestUrl, payLoad).subscribe((data: any) => {
      this.approved = true;
      if (payLoad.isApproved) {
        this.approvalMessage = "Approved";
      } else {
        this.approvalMessage = "Denied";
      }


      this.getRequestedInfo(this.viewRequestedInfo.logsId);
      this.getDashboardInfo(this.paginations.dashboard.currentPage, this.paginations.dashboard.itemsPerPage);
      this.spinner = false;



    }, (err: any) => {
      this.spinner = false;
      this.dialog.open(ErrorHandlerComponent, {
        width: '280px',
        data: {
          message: "Some Error Occured",
        }
      })
    })

    
  }


  /**
   * Get Dashboard Info
   **/

  getLocationInfo(page: any, size: any) {
    let payload: any;
    this.spinner = true;

    if(this.applied_Filters_Location.length > 0){
      payload = {
        "siteCode": this.searchLocationCode,
        "showCriticalSite": (this.applied_Filters_Location.findIndex(obj => obj.value == 1) >= 0) ? true : false,
        "showPendingSite": (this.applied_Filters_Location.findIndex(obj => obj.value == 2) >= 0) ? true : false,
        "showPendingApprovalSite": (this.applied_Filters_Location.findIndex(obj => obj.value == 3) >= 0) ? true : false,
        "showCertifiedSite": (this.applied_Filters_Location.findIndex(obj => obj.value == 4) >= 0) ? true : false,
        "showAllSite": true,
        "page": page,
        "size": size
      };
    } else {
      payload = {
        "siteCode": this.searchLocationCode,
        "showCriticalSite": false,
        "showPendingSite": false,
        "showPendingApprovalSite": false,
        "showCertifiedSite": false,
        "showAllSite": true,
        "page": page,
        "size": size
      };
    }
    if(payload.showCriticalSite || payload.showPendingSite || payload.showPendingApprovalSite || payload.showCertifiedSite){
      payload.showAllSite = false;
    }
    

    let requestURL = urlConstants.baseUrl + urlConstants.getAllLocations;

    

    this.doPOST(requestURL, payload).subscribe((result: any) => {

      this.allLocationData = result || {};
      this.spinner = false;

    },
      (err: any) => {
        console.log(err);
        this.spinner = false;
      })

  }


}
