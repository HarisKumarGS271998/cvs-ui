import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SidenavService {

  constructor() { }

  removeActive(): void{
    let activeRoute = document.querySelectorAll('.activeRoute')
    activeRoute.forEach((node) => {
      node.classList.remove('activeRoute')
    })
  }
  addActive(id: string): void{
    let contactInfoRoute = document.querySelector(id);
    contactInfoRoute?.classList.add('activeRoute')
  }
}
