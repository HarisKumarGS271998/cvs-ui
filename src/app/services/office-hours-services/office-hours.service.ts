import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OfficeHoursService {

  officeHours: any;

  constructor() { }

  initializeOfficeHours(): any{
    // from db
    this.officeHours = [
      {
        day: "Monday",
        open: true,
        officeHoursTime: [
          {
            start: "02.00 am",
            end: "06.00 am"
          }
        ]
      },
      {
        day: "Tuesday",
        open: true,
        officeHoursTime: [
          {
            start: "02.00 am",
            end: "06.00 am"
          }
        ]
      },
      {
        day: "Wednesday",
        open: false,
        officeHoursTime: [
          
        ]
      },
      {
        day: "Thursday",
        open: false,
        officeHoursTime: [

        ]
      },
      {
        day: "Friday",
        open: false,
        officeHoursTime: [
          
        ]
      },
      {
        day: "Saturday",
        open: false,
        officeHoursTime: [
          
        ]
      },
      {
        day: "Sunday",
        open: false,
        officeHoursTime: [
          
        ]
      },
    ]
    return this.officeHours;
  }
}
