import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { urlConstants } from 'src/app/constants/url-constants';
import { Location } from 'src/app/models/Location';
import { MaintenanceHours } from 'src/app/models/MaintenanceHours';
import { officeHoursDto } from 'src/app/models/OfficeHoursDto';
import { siteContact } from 'src/app/models/SiteContact';
import { Survey } from 'src/app/models/Survey';
import { MatDialog } from '@angular/material/dialog';
import { ErrorHandlerComponent } from 'src/app/components/error-handler/error-handler.component'


@Injectable({
  providedIn: 'root'
})
export class SurveyService {

  current: any = "contact";

  surveyInfo: any = {};

  tempFormData: any = {
    siteContactData: siteContact,
    locationData: Location,
    officeHours: [],
    maintenanceHours: MaintenanceHours,
    isMaintenanceHour:false,
    isAdditionalBusiness:false
  };

  
  siteContactData: siteContact;
  locationData: Location;
  officeHours: officeHoursDto[];
  maintenanceHours: MaintenanceHours;

  spinner: boolean = false;

  constructor(
    private _router: Router,
    public _http: HttpClient,
    private dialog: MatDialog
  ) { }


  /******************* Common HTTP Methods ************************/

  /**
   * To GET HTTP Requests
   * @param URL 
   * @param Options 
   * @returns 
   */
  doGET(URL: string, Options?: any) {
    return this._http.get(URL, Options);
  }

  /**
   * To POST HTTP Requests
   * @param URL 
   * @param Payload 
   * @param Options 
   * @returns 
   */
  doPOST(URL: string, Payload: any, Options?: any) {
    return this._http.post(URL, Payload, Options);
  }

  /**
   * To PUT HTTP Requests
   * @param URL 
   * @param Payload 
   * @param Options 
   * @returns 
   */
  doPUT(URL: string, Payload: any, Options?: any) {
    return this._http.put(URL, Payload, Options);
  }

  /**
   * To PATCH HTTP Requests
   * @param URL 
   * @param Payload 
   * @param Options 
   * @returns 
   */
  doPATCH(URL: string, Payload: any, Options?: any) {
    return this._http.patch(URL, Payload, Options);
  }

  /**
   * To DELETE HTTP Requests
   * @param URL 
   * @param Options 
   * @returns 
   */
  doDELETE(URL: string, Options?: any) {
    return this._http.delete(URL, Options);
  }



  /******************* Common Reusable Methods ************************/

  /**
   * Navigate Route to given page
   * @param pageName 
   */
  redirectTo(pageName: any) {
    this._router.navigate([pageName]);
  }


  /**
   * Get Survey Info
   * @param serveyID 
   */
  getSurveyInfo(serveyID: any) {

    let requestURL = urlConstants.baseUrl + urlConstants.surveyUrl + `?id=${serveyID}`;

    this.doGET(requestURL).subscribe((data: any) => {

      this.surveyInfo = data || {};

      console.log(data);


      this.siteContactData = data.siteContact;
      this.locationData = data.site;
      this.officeHours = data.officeHoursDto;
      this.maintenanceHours = data.maintenanceHourDetails;


      if (this.siteContactData.phone) {
        this.siteContactData.phone = this.siteContactData.phone.trim();
      }
      if (this.locationData.mainPhone) {
        this.locationData.mainPhone = this.locationData.mainPhone.trim();
        this.locationData.afterHoursPhone = this.locationData.afterHoursPhone.trim();
      }

      if (!this.surveyInfo.personalInfoCertificationRequired) {
        this.current = "location";
      }
      else if (this.surveyInfo.personalInfoCertificationRequired && !this.surveyInfo.locationInfoCertificationRequired) {
        this.current = "contact";
      }


    },
      (err: any) => {
        this.handleError(err);
      })

  }

  handleError(error: any): void {
    // console.log(error);



    this.dialog.open(ErrorHandlerComponent, {
      width: '280px',
      data: {
        message: error.error,
      },
      disableClose: true
    })
  }



  /**
 * Submit Survey Form data
 */
  submitSurveyForm() {

    let Payload: any = this.surveyInfo || {};
    this.spinner = true;

    // setTimeout(() => {
    //   this.spinner = false;
    // },5000)
    // return


    Payload.siteContact = this.tempFormData.siteContactData;
    Payload.maintenanceHourDetails = this.tempFormData.maintenanceHours;
    Payload.site = this.tempFormData.locationData;
    Payload.officeHoursDto = this.tempFormData.officeHours;

    let requestURL = urlConstants.baseUrl + urlConstants.submitSurveyForm;

    this.doPOST(requestURL, Payload).subscribe((data: any) => {
      // console.log(data);

      this.current = 'submit';
      this.spinner = false;


    }, (err: any) => {
      // console.log(err);

      this.spinner = false;
      this.dialog.open(ErrorHandlerComponent, {
        width: '280px',
        data: {
          message: "Something went wrong, Please try again in sometime!"
        },
        disableClose: false
      })


    });

  }



}
