export class Location {
    businessUnit: String;
    street: String;
    city: String;
    state: String;
    zip: String;
    mainPhone: String;
    afterHoursPhone: String;
    additionalBusinessUnits: any;
    isRedPinValid: boolean;
    pinNotes: String;
    latitude: String;
    longitude: String;
    isStreetUpdated: Boolean;
    isMainPhoneUpdated: Boolean;
    isAfterHoursPhoneUpdated: Boolean
}