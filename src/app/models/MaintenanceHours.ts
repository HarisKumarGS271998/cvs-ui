export class MaintenanceHours {
    maintenanceHourId: number;
    siteId: String;
    isMaintenanceHourAvailable: boolean;
    notes: any;
    isRecurringMaintenance: boolean;
    repeatEvery: number;
    startDate: any;
    repetitiveDay: any;
    repetitiveWeek: number;
    repetitiveWeekOfMonth: number;
    startTime: String;
    endTime: String;
    dailyMaintenances: [
        {
            maintenanceHourId: number;
            date: String;
            repetitiveWeek: number;
            startTime: String;
            endTime: String;
            isUpdated: boolean
        }
    ]
}