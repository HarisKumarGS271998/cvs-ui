import { OfficeHours } from "./OfficeHours";

export class officeHoursDto {
    isUpdated: Boolean;
    siteId: String;
    dayOfWeek: String;
    isOpen: String;
    isActive: String;
    isDeleted: String;
    officeHours: OfficeHours[]
}