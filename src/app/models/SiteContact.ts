export class siteContact {
    email: String;
    name: String;
    nickName: String;
    phone: String;
    otherPhoneNummbers: any;
    additionalQuestionAnswers: any;
    emergencyContacts: any;
    siteId: any;
    isAdditionalQAUpdaed: boolean;
    isEmergencyContactUpdated: boolean;
    isNickNameUpdated: boolean;
    isOtherPhoneUpdated: boolean;
    isPhoneUpdated: boolean;
    notes: any;
    siteAndSiteContactMappingId: any;
}