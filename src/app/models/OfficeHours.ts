export class OfficeHours {
    dayOfWeek: any;
    isActive: Boolean;
    officeHourId: number;
    startTime: String;
    stopTime: String;
    isDeleted: Boolean;
    isOpen: Boolean;
    site: any;
    siteId: String;
}