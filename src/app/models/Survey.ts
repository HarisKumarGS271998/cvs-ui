import { Location } from 'src/app/models/Location';
import { MaintenanceHours } from 'src/app/models/MaintenanceHours';
import { officeHoursDto } from 'src/app/models/OfficeHoursDto';
import { siteContact } from 'src/app/models/SiteContact';

export class Survey {
    "surveyId": number;
    "uniqueId": string;
    "siteId": string;
    "locationInfoCertificationRequired": boolean;
    "personalInfoCertificationRequired": boolean;
    "isLocationCertified": boolean;
    "isPersoanlInfoCertified": boolean;
    "retryCount": number;
    "site": Location;
    "siteContact": siteContact;
    "officeHoursDto": officeHoursDto;
    "maintenanceHourDetails": MaintenanceHours
}

