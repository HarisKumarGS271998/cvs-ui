export const urlConstants = {
    baseUrl: "http://ec2-3-236-218-138.compute-1.amazonaws.com/SiteDev",

    surveyUrl: "/api/Survey/details",
    submitSurveyForm: "/api/Survey/submit",
    dashboardInfo: "/api/admin/requestedUpdates?page={page}&size={size}",
    viewRequestedInfo: "/api/admin/requestedUpdate/detail/{logId}",
    getSiteDetails: "/api/site?siteId={siteId}",
    certifySite: "/api/Survey/certifySite",
    updateRequest: "/api/admin/updateRequest",
    getAllLocations: "/api/admin/fetchSites",
}
// "/api​/admin​/requestedUpdate​/detail​/{logId}"
// /api/admin/updateRequest