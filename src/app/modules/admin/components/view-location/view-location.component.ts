import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { days } from 'src/app/enum/DaysEnum';
import { siteStatus } from 'src/app/enum/SiteStatus';
import { AdminService } from 'src/app/services/admin/admin.service';

@Component({
  selector: 'app-view-location',
  templateUrl: './view-location.component.html',
  styleUrls: ['./view-location.component.scss']
})
export class ViewLocationComponent implements OnInit {

  siteContacts: any[] = [1, 2, 3];

  isSent: boolean = false;

  siteStatus = siteStatus;

  days: any = days;
  payload: any = {
    "siteId": null,
    "siteAndSiteContactMappingId": 0,
    "certifyLocation": false,
    "certifyPersonalInfo": false,
    "both": false,
    "recertifyAll": false
  };

  CertificationStatusMessage: string = "";


  constructor(public _adminService: AdminService) { }

  certificationRadio = new FormControl('both');
  ngOnInit(): void {
    console.log(this._adminService.siteDetails);
    if (this._adminService.siteDetails.groupedOfficeHours) {
      this._adminService?.siteDetails.groupedOfficeHours.sort((a: any, b: any) => {
        return a.dayOfWeek - b.dayOfWeek;
      })
    }
  }

  openDialog(siteAndSiteContactMappingId: any): any {
    this.isSent = false;
    this.payload.siteId = this._adminService.siteDetails.siteId;
    this.payload.siteAndSiteContactMappingId = siteAndSiteContactMappingId;
  }

  openRecertifyAllDialog(): any {
    this.isSent = false;
    this.payload.siteId = this._adminService.siteDetails.siteId;
    this.payload.recertifyAll = true;

    console.log(this.payload);

    console.log(this.isSent);


  }


  resetPayLoad(): void {
    this.payload = {
      "siteId": null,
      "siteAndSiteContactMappingId": null,
      "certifyLocation": false,
      "certifyPersonalInfo": false,
      "both": false,
      "recertifyAll": false
    };
    this.certificationRadio.setValue("both");
    this._adminService.certificationStatus = false;
  }

  sendRecertification(): any {
    if (this.certificationRadio.value == "contact") {
      this.payload.certifyPersonalInfo = true;
    }
    else if (this.certificationRadio.value == "location") {
      this.payload.certifyLocation = true;
    }
    else if (this.certificationRadio.value == "both") {
      this.payload.both = true;
    }
    this.CertificationStatusMessage = this._adminService.certifySite(this.payload)
    this.isSent = true;
  }



  back() {

    this._adminService.isViewLocationDetails = !this._adminService.isViewLocationDetails;
    this._adminService.getLocationInfo(this._adminService.paginations.location.currentPage, this._adminService.paginations.location.itemsPerPage);


  }
}



