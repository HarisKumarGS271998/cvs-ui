import { Component, OnInit } from '@angular/core';
import { AdminService } from 'src/app/services/admin/admin.service';
import { PaginationInstance } from 'ngx-pagination/dist/ngx-pagination.module';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  passenger: any;
  currentPage: number = 1;
  itemsPerPage: number = 5;

  constructor(public _adminService: AdminService, private spinner: NgxSpinnerService) {

   }

  ngOnInit(): void {

    this._adminService.getDashboardInfo(this._adminService.paginations.dashboard.currentPage, this._adminService.paginations.dashboard.itemsPerPage);

    this.spinner.show();

    // setTimeout(() => {
    //   this.spinner.hide();
    // }, 5000);
  }


  selectFilters(selectedItems: any) {
    this._adminService.applied_Filters_Dashboard = selectedItems;

    this._adminService.getDashboardInfo(this._adminService.paginations.dashboard.currentPage, this._adminService.paginations.dashboard.itemsPerPage);

  }


  removeFilters(index: any) {

    this._adminService.applied_Filters_Dashboard.splice(index, 1);
    let filterItem = this._adminService.applied_Filters_Dashboard.map(obj => obj.value);

    this._adminService.selectedFilterItems_Dashboard = filterItem;
  }


  viewRequestedInfo(selectedItem: any) {

    this._adminService.isViewDetailRequest = !this._adminService.isViewDetailRequest;
    this._adminService.getRequestedInfo(selectedItem.logsId);

  }

  changePagination(page: any) {

    this._adminService.paginations.dashboard.currentPage = page;

    this._adminService.getDashboardInfo(this._adminService.paginations.dashboard.currentPage, this._adminService.paginations.dashboard.itemsPerPage);


  }

  formatDate(d: any){
    let date = new Date(d);
    return (((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '/' + ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '/' + date.getFullYear());
  }



}
