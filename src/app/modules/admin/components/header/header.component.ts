import { Component, OnInit } from '@angular/core';
import { AdminService } from 'src/app/services/admin/admin.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(public _adminService: AdminService) { }

  ngOnInit(): void {
  }

  clearAppliedFilter() {
    this._adminService.selectedFilterItems_Dashboard = [];
    this._adminService.selectedFilterItems_Location = [];
    this._adminService.isViewLocationDetails = false;
    this._adminService.isViewDetailRequest = false;

  }
}
