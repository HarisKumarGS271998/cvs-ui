
import { Component, OnInit } from '@angular/core';
import { siteStatus } from 'src/app/enum/SiteStatus';
import {AdminService} from 'src/app/services/admin/admin.service';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.scss']
})
export class LocationsComponent implements OnInit {

  siteStatus = siteStatus;

  passenger: any; 
  currentPage: number = 1;
  itemsPerPage: number = 7;


  constructor(public _adminService:AdminService, private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.spinner.show();
    this._adminService.getLocationInfo(this._adminService.paginations.location.currentPage,this._adminService.paginations.location.itemsPerPage);
  }


  selectFilters(selectedItems: any) {
    this._adminService.applied_Filters_Location = selectedItems;

    // console.log(this._adminService.applied_Filters_Location)
    this._adminService.getLocationInfo(this._adminService.paginations.location.currentPage,this._adminService.paginations.location.itemsPerPage);

  }

  blurChange(): any{
    console.log(this._adminService.searchLocationCode);
    
  }


  removeFilters(index: any) {
   
    this._adminService.applied_Filters_Location.splice(index, 1);
    let filterItem = this._adminService.applied_Filters_Location.map(obj => obj.value);
    
    this._adminService.selectedFilterItems_Location = filterItem;
  }

  viewDetails(locationCode: string): void{
    this._adminService.getSiteDetails(locationCode);
  }

  
  changePagination(page: any){

    this._adminService.paginations.location.currentPage = page;

    this._adminService.getLocationInfo(this._adminService.paginations.location.currentPage,this._adminService.paginations.location.itemsPerPage);


  }
}



