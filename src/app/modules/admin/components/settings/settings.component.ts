import { Component, OnInit } from '@angular/core';
import {AdminService} from 'src/app/services/admin/admin.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  constructor(public _adminService:AdminService) { }

  ngOnInit(): void {
  }

}



