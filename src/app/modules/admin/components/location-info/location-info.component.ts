import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ErrorHandlerComponent } from 'src/app/components/error-handler/error-handler.component';
import { days } from 'src/app/enum/DaysEnum';
import { AdminService } from 'src/app/services/admin/admin.service';


@Component({
  selector: 'app-location-info',
  templateUrl: './location-info.component.html',
  styleUrls: ['./location-info.component.scss']
})
export class LocationInfoComponent implements OnInit {

  days: any = days;
  updateRequestPayload: any = {
    "logId": 0,
    "isApproved": true,
    "updatedLatitude": "",
    "updatedLongitude": ""
  };

  
  constructor(
    public _adminService: AdminService,
    ) { }

  ngOnInit(): void {
  }

  approve(logType: any, logsId: any): void {
    this.updateRequestPayload.logId = logsId;
    this.updateRequestPayload.isApproved = true;
    if (logType == 2) {
      this.updateRequestPayload.updatedLatitude = this._adminService.viewRequestedInfo.newValues?.Latitude;
      this.updateRequestPayload.updatedLongitude = this._adminService.viewRequestedInfo.newValues?.Longitude;
    }
    this._adminService.updateRequest(this.updateRequestPayload)
  }

  deny(logType: any, logsId: any): void {
    this.updateRequestPayload.logId = logsId;
    this.updateRequestPayload.isApproved = false;
    this._adminService.updateRequest(this.updateRequestPayload)

  }

}



