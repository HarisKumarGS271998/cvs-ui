import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin.component';

import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LocationsComponent } from './components/locations/locations.component';
import { SiteContactsComponent } from './components/site-contacts/site-contacts.component';
import { SettingsComponent } from './components/settings/settings.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'locations',
        component: LocationsComponent
      },
      {
        path: 'site-contacts',
        component: SiteContactsComponent
      },
      {
        path: 'setting',
        component: SettingsComponent
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
