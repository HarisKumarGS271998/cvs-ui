import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { AgmCoreModule } from '@agm/core';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { HeaderComponent } from './components/header/header.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LocationsComponent } from './components/locations/locations.component';
import { ViewLocationComponent } from './components/view-location/view-location.component';
import { SiteContactsComponent } from './components/site-contacts/site-contacts.component';
import { LocationInfoComponent } from './components/location-info/location-info.component';
import { SettingsComponent } from './components/settings/settings.component';

import { NgxSpinnerModule } from "ngx-spinner";

@NgModule({
  declarations: [
    AdminComponent,
    HeaderComponent,
    DashboardComponent,
    LocationsComponent,
    ViewLocationComponent,
    SiteContactsComponent,
    LocationInfoComponent,
    SettingsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    AdminRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyB41DRUbKWJHPxaFjMAwdrzWzbVKartNGg'
    }),
    NgxSpinnerModule
  ],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA
  ]
})
export class AdminModule { }
