import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatInputModule } from '@angular/material/input'
import { MatRadioModule } from '@angular/material/radio'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button'
import { MatSelectModule } from '@angular/material/select'
import { MatIconModule } from '@angular/material/icon'
import { MatDatepickerModule } from '@angular/material/datepicker'
import { MatNativeDateModule } from '@angular/material/core';
import { MatSlideToggleModule } from '@angular/material/slide-toggle'
import { MatChipsModule } from '@angular/material/chips'
import { RouterModule } from '@angular/router';

import { NgSelectModule } from '@ng-select/ng-select';
import { MatTooltipModule } from '@angular/material/tooltip'
import { HttpClientModule } from '@angular/common/http';
import { MatDialogModule } from '@angular/material/dialog'
import { MatFormFieldModule } from '@angular/material/form-field';

import { NgxPaginationModule } from 'ngx-pagination';


@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    RouterModule,
    MatInputModule,
    MatRadioModule,
    FormsModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSlideToggleModule,
    MatChipsModule,
    NgSelectModule,
    MatTooltipModule,
    HttpClientModule,
    MatDialogModule,
    MatFormFieldModule,
    NgxPaginationModule
  ],
  exports: [
    MatInputModule,
    MatRadioModule,
    FormsModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSlideToggleModule,
    MatChipsModule,
    NgSelectModule,
    MatTooltipModule,
    HttpClientModule,
    MatDialogModule,
    MatFormFieldModule,
    NgxPaginationModule
  ],

})
export class SharedModule { }
