import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SitecontactComponent } from './survey.component';

describe('SitecontactComponent', () => {
  let component: SitecontactComponent;
  let fixture: ComponentFixture<SitecontactComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SitecontactComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SitecontactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
