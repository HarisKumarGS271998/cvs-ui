import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/modules/shared/shared.module';

import { SurveyRoutingModule } from './survey-routing.module';
import { SitecontactComponent } from './survey.component';

import { SideNavigationComponent } from './components/side-navigation/side-navigation.component';
import { ContactInformationPageComponent } from './components/contact-information-page/contact-information-page.component';
import { SubmitPageComponent } from './components/submit-page/submit-page.component';
import { MaintenanceHoursPageComponent } from './components/maintenance-hours-page/maintenance-hours-page.component';
import { LocationInformationPageComponent } from './components/location-information-page/location-information-page.component';
import { NonRecurringHoursComponent } from './components/non-recurring-hours/non-recurring-hours.component';
import { RecurringHoursComponent } from './components/recurring-hours/recurring-hours.component';
import { OfficeHoursPageComponent } from './components/office-hours-page/office-hours-page.component';
import { BusinessUnitPipe } from 'src/app/pipes/business-unit.pipe';
import { HttpClientModule } from '@angular/common/http';
import { AgmCoreModule } from '@agm/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from "ngx-spinner";
import { NgxMaskModule, IConfig } from 'ngx-mask'

const maskConfig: Partial<IConfig> = {
  validation: false,
};
@NgModule({
  declarations: [
    SitecontactComponent,
    SideNavigationComponent,
    ContactInformationPageComponent,
    SubmitPageComponent,
    MaintenanceHoursPageComponent,
    LocationInformationPageComponent,
    NonRecurringHoursComponent,
    RecurringHoursComponent,
    OfficeHoursPageComponent,
    BusinessUnitPipe,
  ],
  imports: [
    CommonModule,
    SharedModule,
    SurveyRoutingModule,
    HttpClientModule,
    FormsModule, ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyB41DRUbKWJHPxaFjMAwdrzWzbVKartNGg'
    }),
    NgxSpinnerModule,
    NgxMaskModule.forRoot(maskConfig),
  ],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA
  ]
})
export class SitecontactModule { }
