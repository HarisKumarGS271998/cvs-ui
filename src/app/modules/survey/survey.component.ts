import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SurveyService } from 'src/app/services/survey/survey.service';

@Component({
  selector: 'app-sitecontact',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.scss']
})
export class SitecontactComponent implements OnInit {

  constructor(
    public _serveyService: SurveyService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {

    this.route.queryParams.subscribe((p) => {
      let serveyID = p.id;
      this._serveyService.getSurveyInfo(serveyID);
    })

  }

}
