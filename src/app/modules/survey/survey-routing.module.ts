import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SitecontactComponent } from './survey.component';

import { ContactInformationPageComponent } from './components/contact-information-page/contact-information-page.component';
import { SubmitPageComponent } from './components/submit-page/submit-page.component';
import { MaintenanceHoursPageComponent } from './components/maintenance-hours-page/maintenance-hours-page.component';
import { LocationInformationPageComponent } from './components/location-information-page/location-information-page.component';
import { OfficeHoursPageComponent } from './components/office-hours-page/office-hours-page.component';


const routes: Routes = [
  {
    path: '',
    component: SitecontactComponent,
    // children: [
    //   // { path: '', redirectTo: 'contactInformation', pathMatch: 'full' },
    //   {
    //     path: 'contactInformation',
    //     component: ContactInformationPageComponent
    //   },
    //   {
    //     path: 'locationInformation',
    //     component: LocationInformationPageComponent
    //   },
    //   {
    //     path: 'officeHours',
    //     component: OfficeHoursPageComponent
    //   },
    //   {
    //     path: 'maintenanceHours',
    //     component: MaintenanceHoursPageComponent
    //   },
    //   {
    //     path: 'submit',
    //     component: SubmitPageComponent
    //   }
    // ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SurveyRoutingModule { }
