import { Component, OnInit } from '@angular/core';
import { SidenavService } from 'src/app/services/sidenav-services/sidenav.service';
import { SurveyService } from 'src/app/services/survey/survey.service';

@Component({
  selector: 'app-submit-page',
  templateUrl: './submit-page.component.html',
  styleUrls: ['./submit-page.component.scss']
})
export class SubmitPageComponent implements OnInit {

  constructor(
    private sideNavService: SidenavService,
    public _serveyService: SurveyService
  ) { }

  ngOnInit(): void {
    this.sideNavService.addActive('#contact_info_route');
    this.sideNavService.addActive('#location_info_route');
    this.sideNavService.addActive('#mainrenance_hours_route');
    this.sideNavService.addActive('#submit_route');
    this.sideNavService.addActive('#office_hours_route');

    
    // this._serveyService.submitSurveyForm();

  }


}
