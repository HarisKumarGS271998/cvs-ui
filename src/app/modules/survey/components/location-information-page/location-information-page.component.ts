import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SidenavService } from 'src/app/services/sidenav-services/sidenav.service';
import { SurveyService } from 'src/app/services/survey/survey.service';

@Component({
  selector: 'app-location-information-page',
  templateUrl: './location-information-page.component.html',
  styleUrls: ['./location-information-page.component.scss']
})
export class LocationInformationPageComponent implements OnInit {
  @ViewChild('f') f: ElementRef;

  @Input() locationData: any = {};

  businessUnitsOptions: String[] = ["bu 1", "bu 2", "bu 3", "bu 4"];

  businessUnitsOptions1: any[] = [
    { lable: "bu 1", value: "bu 1" },
    { lable: "bu 2", value: "bu 2" },
    { lable: "bu 3", value: "bu 3" },
    { lable: "bu 4", value: "bu 4" },
  ];

  selectedCar: number;

  cars = [
    { id: 1, name: 'Volvo' },
    { id: 2, name: 'Saab' },
    { id: 3, name: 'Opel' },
    { id: 4, name: 'Audi' },
  ];


  additionalBusiness: boolean;
  selectedBusinessnits: any;
  editted: boolean = false;
  searchText: String;

  zoom = 8;
  lat: number;
  long: number;

  additionalBusinessUnitSelectOpen: boolean = true;

  constructor(
    private sideNavService: SidenavService,
    private route: ActivatedRoute,
    public _serveyService: SurveyService
  ) { }

  ngOnInit(): void {


    this.selectedBusinessnits = [];

    if (this.locationData.additionalBusinessUnits == null) {
      this.locationData.additionalBusinessUnits = [];
    }

    if (this.locationData.additionalBusinessUnits.length == 0) {
      this.additionalBusiness = false;
      
    } else {
      this.additionalBusiness = true
      
    }

    this.lat = Number(this.locationData.latitude);
    this.long = Number(this.locationData.longitude);

  }


  ngAfterContentChecked(): void {
    this.sideNavService.removeActive();
    this.sideNavService.addActive('#contact_info_route');
    this.sideNavService.addActive('#location_info_route');

    this.additionalBusiness = this._serveyService.tempFormData.isAdditionalBusiness;


  }

  goback(): void {
    this._serveyService.current = 'contact';
  }

  update(): void {
    this._serveyService.current = 'officehours'
  }

  next(): void {
    this._serveyService.current = 'officehours';

    this._serveyService.tempFormData.locationData = this.locationData || {};

  }

  locationEditted(event: any) {
    if (!event.value) {
      this.editted = true;
    }
    else {
      this.editted = false;
    }
  }

  // ed(e: any, controlName: any): void{
  //   if(e.target.value !== this.edittable[controlName]){
  //     this.editted = true;
  //   }else {
  //     this.editted = false;
  //   }
  // }

  addBusiSelectOpen(): void {
    this.additionalBusinessUnitSelectOpen = !this.additionalBusinessUnitSelectOpen;
    this.businessUnitsOptions = this.businessUnitsOptions;
  }

  removeChip(element: any): void {
    let array: any = this.locationData.additionalBusinessUnits as String[];
    let i = array.indexOf(element);
    if (i !== -1) {
      array.splice(i, 1);
    }

    this.locationData.additionalBusinessUnits = (array.length > 0) ? array : null;
    console.log(this.locationData.additionalBusinessUnits)
  }


  streetUpdated(): void {
    this.locationData.isStreetUpdated = true;
  }

  mainPhoneUpdated(): void {
    this.locationData.isMainPhoneUpdated = true;
  }

  afterHoursPhoneUpdated(): void {
    this.locationData.isAfterHoursPhoneUpdated = true
  }

  changed(): void {
    this.locationData.isAdditionalBUUpdated = true;
  }

  additionalBusinessRadioChange(event: any): void {
    if (event == 'no') {
      this.locationData.additionalBusinessUnits = [];
    }

    this._serveyService.tempFormData.isAdditionalBusiness = this.additionalBusiness;

  }


  onSubmit() {

    if (this.additionalBusiness) {

      if ((this.locationData.additionalBusinessUnits.length == 0 || this.locationData.additionalBusinessUnits.length > 3)) {
        return;
      }

    }
    else if (!(this.locationData.street && this.locationData.mainPhone && this.locationData.mainPhone.length >= 10 && this.locationData.afterHoursPhone && this.locationData.afterHoursPhone.length >= 10)) {
      return;
    }
    else if (!this.locationData.isRedPinValid) {

      if (!(this.locationData.pinNotes && this.locationData.pinNotes.length >= 10))
        return;

    }

    // alert("asd");

    this.next();
  }



}
