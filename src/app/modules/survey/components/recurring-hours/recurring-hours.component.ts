import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { hoursConstant } from 'src/app/constants/hours';
import { days } from 'src/app/enum/DaysEnum';
import { weekDays } from 'src/app/enum/WeekDay';
import { weekOfMonth } from 'src/app/enum/WeekOfMonth';
import { dayOfMonth } from 'src/app/constants/dayOfMonth';
import { repeatEveryEnum } from 'src/app/enum/RepeatEvery';

@Component({
  selector: 'app-recurring-hours',
  templateUrl: './recurring-hours.component.html',
  styleUrls: ['./recurring-hours.component.scss']
})
export class RecurringHoursComponent implements OnInit {

  @Output() valueChange = new EventEmitter();

  recurringHours: any;
  hours: any;
  activeDay: any;
  currentFromTime: any = '';
  currentToTime: any = '';
  clickedDayDetails: any;

  currentView: string = "week"

  daysArray: any = [];

  mh: any = [];

  days: any;

  weekOfDays: any;
  weekOfMonths: any;
  dayOfMonth = dayOfMonth;

  repeatEvery = repeatEveryEnum;

  minDate: any;
  

  @Input() dailyMaintenances: any;
  constructor(
  ) { }

  ngOnInit(): void {

    this.hours = hoursConstant;

    this.days = days;

    this.weekOfDays = weekDays;
    this.weekOfMonths = weekOfMonth;

    if(this.dailyMaintenances.dailyMaintenances == null){
      this.dailyMaintenances.dailyMaintenances = [];
    }
    this.minDate = new Date();

    if(this.dailyMaintenances.endTime != null && this.dailyMaintenances.startTime != null){
      this.dailyMaintenances.endTime = this.dailyMaintenances.endTime.trim();
      this.dailyMaintenances.startTime = this.dailyMaintenances.startTime.trim()
    }
    
    this.daysArray = [
      {
        dayOfWeek: 1,
        day: "Monday",
        isActive: false
      },
      {
        dayOfWeek: 2,
        day: "Tuesday",
        isActive: false
      },
      {
        dayOfWeek: 3,
        day: "Wednesday",
        isActive: false
      },
      {
        dayOfWeek: 4,
        day: "Thursday",
        isActive: false
      },
      {
        dayOfWeek: 5,
        day: "Friday",
        isActive: false
      },
      {
        dayOfWeek: 6,
        day: "Saturday",
        isActive: false
      },
      {
        dayOfWeek: 7,
        day: "Sunday",
        isActive: false
      }
    ]

    this.setActiveDayOfWeek();
  }

  setActiveDayOfWeek(): void{
    this.dailyMaintenances.dailyMaintenances.map((m: any) => {
      this.daysArray.map((day: any) => {
        if((m.repetitiveWeek == day.dayOfWeek)){
          day.isActive = true;
        }
      })
    })
    console.log(this.daysArray);
  }

  addOrRemove(day: any){
    if(!day.isActive){
      day.isActive = true;
      this.dailyMaintenances.dailyMaintenances.push({
        date: null,
        maintenanceHourId: 0,
        startTime: "",
        endTime: "",
        repetitiveWeek: day.dayOfWeek,
        isUpdated: true
      })
      this.weekDataChanged();
    }
    else if(day.isActive){
      let index = this.dailyMaintenances.dailyMaintenances.findIndex((a: any) => a.repetitiveWeek == day.dayOfWeek);
      this.dailyMaintenances.dailyMaintenances.splice(index, 1);
      day.isActive = false;
      this.weekDataChanged();
    }
  }

  getEndHours(startTime: any): any{
    let ho = this.hours;
    let iStartTime = ho.indexOf(startTime);
    ho = ho.filter((h: any, index: any) => {
      if(index > iStartTime){
        return h;
      }
    })
    return ho;
  }

  updateChanged(event?: any){
    this.valueChange.emit(true);
    // this.dailyMaintenances.repetitiveDay = new Date(event).getDate();
  }

  weekDataChanged(): void{
    this.valueChange.emit(true);
  }

  changedView(): void{
    this.dailyMaintenances.dailyMaintenances = [];
    this.dailyMaintenances.startDate = null;
    this.dailyMaintenances.startTime = null;
    this.dailyMaintenances.endTime = null;
    this.dailyMaintenances.repetitiveWeek = null;
    this.dailyMaintenances.repetitiveWeekOfMonth = null;
    this.dailyMaintenances.repetitiveDay = null;
  }

}
