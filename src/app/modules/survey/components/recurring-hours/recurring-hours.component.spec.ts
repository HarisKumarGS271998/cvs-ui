import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecurringHoursComponent } from './recurring-hours.component';

describe('RecurringHoursComponent', () => {
  let component: RecurringHoursComponent;
  let fixture: ComponentFixture<RecurringHoursComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecurringHoursComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecurringHoursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
