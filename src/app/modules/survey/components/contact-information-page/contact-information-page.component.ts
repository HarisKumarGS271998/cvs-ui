import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SidenavService } from 'src/app/services/sidenav-services/sidenav.service';
import { SurveyService } from 'src/app/services/survey/survey.service';
import { COMMA, ENTER } from '@angular/cdk/keycodes'
@Component({
  selector: 'app-contact-information-page',
  templateUrl: './contact-information-page.component.html',
  styleUrls: ['./contact-information-page.component.scss']
})
export class ContactInformationPageComponent implements OnInit {
  @ViewChild('f') f: ElementRef;

  @Input() siteContactData: any = {};

  emergency: any;

  readonly separatorKeysCodes = [ENTER, COMMA] as const;

  // editted: false;

  constructor(
    private router: Router,
    private sideNavService: SidenavService,
    public _serveyService: SurveyService
  ) { }

  ngOnInit(): void {
    
    if (this.siteContactData.emergencyContacts != null) {
      this.emergency = true;
    } else {
      this.emergency = false;
    }

    this.siteContactData.additionalQuestionAnswers.map((ques: any) => {
      if (ques.answerType == 1) {
        ques.answer = "no"
      }
    })

    console.log(this.siteContactData.additionalQuestionAnswers);
  }


  ngAfterContentChecked(): void {
    this.sideNavService.removeActive();
    this.sideNavService.addActive('#contact_info_route');
  }


  add(event: any): void {
    const value = (event.value || '').trim();
    if (value) {
      this.siteContactData.otherPhoneNummbers.push(value)
    }
    this.siteContactData.isOtherPhoneUpdated = true;
    event.chipInput!.clear();
  }

  remove(val: any): void {
    const index = this.siteContactData.otherPhoneNummbers.indexOf(val);

    if (index >= 0) {
      this.siteContactData.otherPhoneNummbers.splice(index, 1);
    }
  }


  phoneUpdated(): void {
    this.siteContactData.isPhoneUpdated = true;
  }
  nickNameUpdate(): void {
    this.siteContactData.isNickNameUpdated = true;
  }

  emergencyContactUpdated(): void {
    this.siteContactData.isEmergencyContactUpdated = true;
  }

  // emergencyChange(event: any): void {
  //   if (event.value == "no") {
  //     this.emergency = "no";
  //   } else {
  //     this.emergency = "yes";
  //   }
  // }

  updatedAdditionalQues(index: any) {
    this.siteContactData.isAdditionalQAUpdaed = true;
    this.siteContactData.additionalQuestionAnswers[index].isUpdated = true;
  }

  submitContactForm(): void {


    if (this._serveyService.surveyInfo.personalInfoCertificationRequired && this._serveyService.surveyInfo.locationInfoCertificationRequired) {
      this._serveyService.current = "location";
      console.log(this.siteContactData);

    }
    else if (this._serveyService.surveyInfo.personalInfoCertificationRequired && !this._serveyService.surveyInfo.locationInfoCertificationRequired) {
      this._serveyService.current = "submit";
    }

    this._serveyService.tempFormData.siteContactData = this.siteContactData || {};


  }

  emergencyUpdated(event: any): void {
    if (!event) {
      this.siteContactData.emergencyContacts = null;
    }
  }



  onSubmit() {


    if (!(this.siteContactData.phone && (this.siteContactData.phone.length >= 10))) {
      return;
    }
    else if (this.emergency) {
      if (!this.siteContactData.emergencyContacts || !this.siteContactData.emergencyContacts.toString().match("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")
        || this.siteContactData.emergencyContacts.split(',').length > 1) {
        return;
      }
    }

    // console.log(this.siteContactData.emergencyContacts.split(','));
    

    this.submitContactForm();
  }


}
