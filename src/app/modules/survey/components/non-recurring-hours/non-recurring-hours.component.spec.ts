import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NonRecurringHoursComponent } from './non-recurring-hours.component';

describe('NonRecurringHoursComponent', () => {
  let component: NonRecurringHoursComponent;
  let fixture: ComponentFixture<NonRecurringHoursComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NonRecurringHoursComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NonRecurringHoursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
