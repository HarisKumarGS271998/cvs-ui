import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { hoursConstant } from 'src/app/constants/hours';

@Component({
  selector: 'app-non-recurring-hours',
  templateUrl: './non-recurring-hours.component.html',
  styleUrls: ['./non-recurring-hours.component.scss']
})
export class NonRecurringHoursComponent implements OnInit {
  @Output() valueChange = new EventEmitter();

  nonRecurringHours: any;
  hours: any;
  minDate: any;

  @Input() dailyMaintenances: any[];
  constructor() { }

  ngOnInit(): void {
    // this.nonRecurringHours = [
    //   {
    //     id: 1,
    //     date: '',
    //     fromTime: '02.00 am',
    //     toTime: ''
    //   },
    // ]
    this.hours = hoursConstant;
    this.minDate = new Date();

    console.log(this.dailyMaintenances);
    // this.dailyMaintenances = [];
    if(this.dailyMaintenances.length == 0){
      this.dailyMaintenances.push({
        maintenanceHourId: 0,
        date: '',
        startTime: '',
        endTime: ''
      })
    }

  }

  getEndHours(startTime: any): any {
    let ho = this.hours;
    ho = ho.filter((h: any, index: any) => {
      if (h != startTime) {
        return h;
      }
    })
    return ho;
  }

  addNewRec(): void {
    this.dailyMaintenances.push({
      maintenanceHourId: 0,
      date: '',
      startTime: '',
      endTime: ''
    })
  }

  delete(index: any): void {
    this.dailyMaintenances.splice(index, 1);
    // console.log(this.nonRecurringHours)
  }




  changeDate(event: any, index: any) {
    
    this.dailyMaintenances[index].isUpdated = true;

    this.valueChange.emit(true);
    
  }
}
