import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MaintenanceHoursPageComponent } from './maintenance-hours-page.component';

describe('MaintenanceHoursPageComponent', () => {
  let component: MaintenanceHoursPageComponent;
  let fixture: ComponentFixture<MaintenanceHoursPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MaintenanceHoursPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MaintenanceHoursPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
