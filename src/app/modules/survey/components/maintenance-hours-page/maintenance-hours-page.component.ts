import { Location } from '@angular/common';
import { Component, OnInit, ViewChild, AfterViewInit, Input, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SidenavService } from 'src/app/services/sidenav-services/sidenav.service';
import { SurveyService } from 'src/app/services/survey/survey.service';
import { NonRecurringHoursComponent } from '../non-recurring-hours/non-recurring-hours.component';
import { RecurringHoursComponent } from '../recurring-hours/recurring-hours.component';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-maintenance-hours-page',
  templateUrl: './maintenance-hours-page.component.html',
  styleUrls: ['./maintenance-hours-page.component.scss']
})
export class MaintenanceHoursPageComponent implements AfterViewInit {
  @Input() maintenanceHours: any = {};
  @ViewChild('f') f: ElementRef;


  maintenanceHourRadio: any;
  recurruingMaintenanceHoursRadio: any;

  invalidHours: any;

  validPage = true;
  warningMsg = false;

  dataFromNonRecurring: any;
  dataFromRecurring: any;

  @ViewChild(NonRecurringHoursComponent, { static: false }) nonRecurringChild: NonRecurringHoursComponent;
  @ViewChild(RecurringHoursComponent, { static: false }) recurringData: RecurringHoursComponent;

  constructor(
    private sideNavService: SidenavService,
    public _serveyService: SurveyService,
    private spinner: NgxSpinnerService
  ) { }

  ngAfterViewInit(): void {
    this.spinner.show();


    this.sideNavService.removeActive();
    this.sideNavService.addActive('#contact_info_route');
    this.sideNavService.addActive('#location_info_route');
    this.sideNavService.addActive('#mainrenance_hours_route');
    this.sideNavService.addActive('#office_hours_route');

    // debugger;
    if (!this.maintenanceHours.hasOwnProperty('isMaintenanceHourAvailable')) {
      this.maintenanceHours = {
        "isMaintenanceHourAvailable": false,
        "isUpdated": true,
        "dailyMaintenances": [],
        "isRecurringMaintenance": false,
        "notes": null
      };
      this.maintenanceHours.isMaintenanceHourAvailable=this._serveyService.tempFormData.isMaintenanceHour;

    }
    console.log(this.maintenanceHours);


  }

  checkInvalid(): void {
    console.log("checking...!");
    

    // 

    if (!this.maintenanceHours.isRecurringMaintenance) {
      this.invalidHours = this.maintenanceHours.dailyMaintenances.filter((obj: any) => !obj.date || !(obj.startTime || obj.endTime)).length || null;
    }
    else {
      console.log("checking for recurring...");
      if(!this.maintenanceHours.repeatEvery){
        this.invalidHours = true;
      } else {
        this.invalidHours = false;
      }
      if(this.maintenanceHours?.repeatEvery == 1){
        if(!this.maintenanceHours.startDate || !this.maintenanceHours.repetitiveDay || !this.maintenanceHours.repetitiveWeek || !this.maintenanceHours.repetitiveWeekOfMonth || !this.maintenanceHours.endTime || !this.maintenanceHours.startTime){
          this.invalidHours = true;
        } else {
          this.invalidHours = false;
        }
      }
      else if(this.maintenanceHours?.repeatEvery == 2) {
        if(!this.maintenanceHours.startDate){
          this.invalidHours = true;
        } else {
          this.invalidHours = false;
        }
      }

      console.log(this.invalidHours);
      
      // if (this.maintenanceHours.startDate == null) {
      //   this.invalidHours = true;
      // }
      // else {
      //   this.invalidHours = this.maintenanceHours.dailyMaintenances.filter((obj: any) => !(obj.startTime || obj.endTime)).length || null;
      // }
    }

    // this.invalidHours = this.maintenanceHours.dailyMaintenances.find((main: any) => {
    //   if (main.startTime == "" || main.endTime == "" || main.date == ) {
    //     return main;
    //   }
    // })

    if (this.invalidHours) {
      this.validPage = false;
    } else {
      this.validPage = true;
    }

    console.log(this.validPage);
    

  }

  next(): void {
    this.checkInvalid();


    if (!this.validPage) {
      this.warningMsg = true;
      setTimeout(() => {
        this.warningMsg = false;
      }, 2000)
    }

    console.log(this.maintenanceHours);

    // return;
    this._serveyService.tempFormData.maintenanceHours = this.maintenanceHours || {};

    if (this.validPage) {
      this._serveyService.submitSurveyForm();
    }
  }

  onSubmit(): void{
    console.log("submitting");
    if(!this.maintenanceHours.isMaintenanceHourAvailable){
      if(!this.maintenanceHours.notes || this.maintenanceHours?.notes.length <= 10){
        return;
      } else {
        this.next();
      }
    } else {
      this.next();
    }
  }


  back(): void {
    this._serveyService.current = 'officehours'
  }



  valueChanged(event?: any) {
    this.maintenanceHours.isUpdated = true;

    console.log(this.maintenanceHours);

    this._serveyService.tempFormData.isMaintenanceHour = this.maintenanceHours.isMaintenanceHourAvailable;

    if(event){
      this.maintenanceHours.notes = null;
    } else {
      this.maintenanceHours.dailyMaintenances = []
    }
    
  }
  

  recurringRadioChanged(event?: any) {
    this.maintenanceHours.isUpdated = true;

    this.maintenanceHours.dailyMaintenances = [];
    this.maintenanceHours.startDate = null;
    this.maintenanceHours.startTime = null;
    this.maintenanceHours.endTime = null;
    this.maintenanceHours.repetitiveWeek = null;
    this.maintenanceHours.repetitiveWeekOfMonth = null;
    this.maintenanceHours.repetitiveDay = null;
    this.maintenanceHours.repeatEvery = null;
    console.log(this.maintenanceHours);

  }



}
