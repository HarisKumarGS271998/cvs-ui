import { Location } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { hoursConstant } from 'src/app/constants/hours';
import { days } from 'src/app/enum/DaysEnum';
// import { OfficeHoursService } from 'src/app/services/office-hours-services/office-hours.service';
import { SidenavService } from 'src/app/services/sidenav-services/sidenav.service';
import { SurveyService } from 'src/app/services/survey/survey.service';

@Component({
  selector: 'app-office-hours-page',
  templateUrl: './office-hours-page.component.html',
  styleUrls: ['./office-hours-page.component.scss']
})
export class OfficeHoursPageComponent implements OnInit {
  @Input() officeHours: any = {};

  hours: any;
  valid = true;
  warningMsg = false;
  days: any;

  constructor(
    private sideNavService: SidenavService,
    // private officeHoursService: OfficeHoursService,
    public _serveyService: SurveyService
  ) { }

  ngOnInit(): void {
    console.log(this.officeHours);

    this.sideNavService.removeActive();
    this.sideNavService.addActive('#contact_info_route');
    this.sideNavService.addActive('#location_info_route');
    this.sideNavService.addActive('#office_hours_route');

    this.days = days;

    this.hours = hoursConstant;

    // this.officeHours = this.officeHoursService.initializeOfficeHours();

    this.officeHours.sort((a: any, b: any) => {
      return a.dayOfWeek - b.dayOfWeek;
    })

    // console.log(this.officeHours);


  }

  send(): void {
    console.log(this.officeHours)
  }

  addTimings(i: any): void {
    if(this.officeHours[i].officeHours.length < 3) {
      this.officeHours[i].officeHours.push({
        officeHourId: 0,
        startTime: "",
        stopTime: "",
        site: null,
        siteId: this._serveyService.siteContactData.siteId,
        isOpen: false,
        isDeleted: false,
        isActive: false,
        dayOfWeek: null
      })
    }
  }

  deleteTimings(id: any): void {
    this.officeHours[id].officeHours.pop();
  }

  onCheck(event: any, id: any): void {
    this.officeHours[id].isUpdated = true;
    if (event.checked && this.officeHours[id].officeHours.length == 0) {
      this.officeHours[id].officeHours.push({
        officeHourId: 0,
        startTime: "",
        stopTime: "",
        site: null,
        siteId: this._serveyService.siteContactData.siteId,
        isOpen: false,
        isDeleted: false,
        isActive: false,
        dayOfWeek: null
      })
    }
    if (!event.checked) {
      this.officeHours[id].officeHours = [];
    }
  }

  checkEmptyOfficeHoursTime(): void {
    let invalidOfficeHour = null;
    for (let i = 0; i < this.officeHours.length; i++) {
      if (this.officeHours[i].officeHours.length > 0 && this.officeHours[i].isOpen) {
        invalidOfficeHour = this.officeHours[i].officeHours.find((oft: any) => {
          if (oft.startTime == "" || oft.stopTime == "" || oft.startTime == null || oft.stopTime == null) {
            return oft;
          }
        });
        if (invalidOfficeHour) {
          this.valid = false;
          break;
        }
        this.valid = true;
      }
    }
  }

  getHours(mainIndex: any, subIndex: any): any {
    if (subIndex == 0) {
      return this.hours;
    }

    let ho = this.hours;
    if (subIndex > 0) {
      let startTime = this.officeHours[mainIndex].officeHours[subIndex - 1].startTime;
      let endTime = this.officeHours[mainIndex].officeHours[subIndex - 1].stopTime;
      let iStartTime = this.hours.indexOf(startTime);
      let iEndTime = this.hours.indexOf(endTime);
      ho = ho.filter((h: any, index: any) => {
        if (index >= iEndTime) {
          return h
        }
      })

      return (ho);
    }
    return [];
  }

  getEndHours(startTime: any, mainIndex: any, subIndex: any): any {
    let ho = this.hours;
    let iStartTime = ho.indexOf(startTime);
    ho = ho.filter((h: any, index: any) => {
      if (index >= iStartTime) {
        return h;
      }
    })
    return ho;
  }

  prev(): void {
    this._serveyService.current = 'location';
  }

  next(): void {

    this.checkEmptyOfficeHoursTime();
    // console.log(this.officeHours)
    if (!this.valid) {
      this.warningMsg = true;
      setTimeout(() => {
        this.warningMsg = false;
      }, 2000)
    }
    if (this.valid) {
      this._serveyService.current = 'maintenancehours'
    }

    this._serveyService.tempFormData.officeHours = this.officeHours || {};


  }

  check(off: any) {
    console.log(off);

  }

  updated(id: any): void {
    this.officeHours[id].isUpdated = true;
    console.log(this.officeHours);

  }

}
