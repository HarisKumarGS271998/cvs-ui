import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-redirect',
  templateUrl: './redirect.component.html',
  styleUrls: ['./redirect.component.scss']
})
export class RedirectComponent implements OnInit {

  routeId: any = null;

  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {

    this.route.queryParams.subscribe((p) => {
      this.routeId = p.id;
    })
    // console.log(this.routeId)

    if(this.routeId == null || this.routeId == undefined){
      this.router.navigate(['admin']);
    } else {
      this.router.navigate([`/survey`], {queryParams: {id: this.routeId}})
    }
    
  }

}
