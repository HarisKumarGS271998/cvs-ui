import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'businessUnit'
})
export class BusinessUnitPipe implements PipeTransform {

  transform(units: any[], searchText: String): any {
    if(!units){
      return [];
    }
    if(!searchText){
      return units;
    }
    searchText = searchText.toLocaleLowerCase();
    return units.filter((unit) => {
      return unit.toLocaleLowerCase().includes(searchText);
    })
  }


}
