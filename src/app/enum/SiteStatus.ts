export enum siteStatus {
    None = 0,
    Critical = 1,
    Pending = 2,
    RequestedForApproval = 3,
    Certified = 4
}